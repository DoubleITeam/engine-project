<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Search For Holidays</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100i,300,400&display=swap" rel="stylesheet">

</head>
<body>
<div id="bg">
    <div id="more-popup">
        <div class="container">
            <div class="row more-title-bg"><span class="more-title">Berlin</span></div>
            <div style="margin-top: 30px;" class="row">
                <div class="col-md-6 text-black">
                    <div>
                        <div class="row"><span class="stat-label">Avg Temp:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-good" style="width: 80%"><span class="stat-desc">Good</span></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="row"><span class="stat-label">Monuments:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-good" style="width: 70%"><span class="stat-desc">A Lot</span></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="row"><span class="stat-label">Nightlife:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-bad" style="width: 20%"><span class="stat-desc">Poor</span></div></div>
                        </div>
                    </div>

                    <div>
                        <div class="row"><span class="stat-label">Beach access:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-medium" style="width: 50%"><span class="stat-desc">Average</span></div></div>
                        </div>
                    </div>

                    <input type="button" class="more-button" value="Read more">
                </div>
                <div class="col-md-6 text-black">
                    <div>
                        <div class="row"><span class="stat-label">Internet Access:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-good" style="width: 80%"><span class="stat-desc">Great</span></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="row"><span class="stat-label">Toursts paths:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-bad" style="width: 20%"><span class="stat-desc">Few</span></div></div>
                        </div>
                    </div>

                    <div>
                        <div class="row"><span class="stat-label">English speaking:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-medium" style="width: 40%"><span class="stat-desc">Okay</span></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="row"><span class="stat-label">City crowding:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-bad" style="width: 90%"><span class="stat-desc">High</span></div></div>
                        </div>
                    </div>
                    <input type="button" class="more-button" value="Buy now">

                </div>
                </div>
            </div>

            <input type="button" id="close-button" onclick="closePopup()" value="">
        </div>
    </div>
</div>
<script>
    function openTripType(){
        document.getElementById("oneTripType").style.height="130px";
    }

    function option1(){
        document.getElementById("option1").className="oneTripTypeOption chosedOption";
        document.getElementById("option2").className="oneTripTypeOption oneTripOptionCenter";
        document.getElementById("option3").className="oneTripTypeOption";

        document.getElementById("option1Img").src="images/mapW.png";
        document.getElementById("option2Img").src="images/beach.png";
        document.getElementById("option3Img").src="images/glass-cocktail.png";

        openCityPicker();
    }

    function option2(){
        document.getElementById("option2").className="oneTripTypeOption chosedOption oneTripOptionCenter";
        document.getElementById("option1").className="oneTripTypeOption";
        document.getElementById("option3").className="oneTripTypeOption";

        document.getElementById("option1Img").src="images/map.png";
        document.getElementById("option2Img").src="images/beachW.png";
        document.getElementById("option3Img").src="images/glass-cocktail.png";

        openCityPicker();
    }

    function option3(){
        document.getElementById("option3").className="oneTripTypeOption chosedOption";
        document.getElementById("option2").className="oneTripTypeOption oneTripOptionCenter";
        document.getElementById("option1").className="oneTripTypeOption";

        document.getElementById("option1Img").src="images/map.png";
        document.getElementById("option2Img").src="images/beach.png";
        document.getElementById("option3Img").src="images/glass-cocktailW.png";

        openCityPicker();
    }


    function openCityPicker(){
        document.getElementById("oneTripWhereFrom").style.height="110px";
    }

</script>
<!--1140-->
<div id="oneSearchBlock">
    <div id="oneSearchForm">
        <div class="container">
            <div class="row">
                <div class="col">
                    <img src="images/logo.png" id="logo" >
                </div>
            </div>
        </div>
        <div id="oneSearchDates">
            <span class="oneSpanSearch">When are you planning a trip?</span>
            <div id="oneSearchDatesInputs">
                <input type="date">
                <input onclick="openTripType()" type="date">
            </div>
        </div>
        <div id="oneTripType">
            <span class="oneSpanSearch">What do you want to do?</span>
            <div id="oneAllTripTypes">
                <div onclick="option1()" id="option1" class="oneTripTypeOption">
                    <img id="option1Img" src="images/map.png">
                    <span>Active recreation</span>
                </div>
                <div onclick="option2()" id="option2" class="oneTripTypeOption oneTripOptionCenter">
                    <img id="option2Img" src="images/beach.png">
                    <span>Sunbath holidays</span>
                </div>
                <div onclick="option3()" id="option3" class="oneTripTypeOption">
                    <img id="option3Img" src="images/glass-cocktail.png">
                    <span>Nightlife</span>
                </div>
            </div>
        </div>
        <div id="oneTripWhereFrom">
            <span class="oneSpanSearch">Where do you fly from?</span>
            <div id="oneWhereFromPicker">
                <input id="oneCityInput" list="cityname" class="oneWhereInput" placeholder="Enter your city" type="text">
                <datalist id="cityname">
                    <option value="Boston">
                    <option value="Cambridge">
                     <option value="Warsaw"></option>
                    <option value="Warswage"></option>
                    <option value="Warengow"></option>
                    <option value="Warszawa"></option>
                </datalist>
            </div>
        </div>
    </div>

</div>
    <div class="container" id="content" >
            <div class="row" style="margin-top: 30px; z-index: -1;">
                <div class="col-md-12">
                    <span class="result-title">
                        We recommend you these directions:
                    </span>
                    <label class="checkbox-container">Only direct flights
                        <input type="checkbox" checked="checked">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <div class="row separator"></div>
            <div class="row city">
                <div class="col-md-8" id="berlin">
                    <div class="row">
                        <span class="city-title">Berlin</span>
                    </div>
                    <div class="row">
                        <span class="flight-duration">Flight duration: 3h</span>
                    </div>
                    <div class="internal-separator"></div>
                    <div class="row">
                        <span class="cost">Flight cost: 2000 PLN</span>
                    </div>
                    <div class="row">
                        <span class="cost">Accomodation: avg. 1000PLN/Night</span>
                    </div>
                </div>
                <div class="col-md-4 text-black">
                    <div>
                        <div class="row"><span class="stat-label">Avg Temp:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-good" style="width: 80%"><span class="stat-desc">Good</span></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="row"><span class="stat-label">Monuments:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-good" style="width: 70%"><span class="stat-desc">A Lot</span></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="row"><span class="stat-label">Nightlife:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-bad" style="width: 20%"><span class="stat-desc">Poor</span></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="row"><span class="stat-label">Beach access:</span></div>
                        <div class="row">
                            <div class="value-max-bg"><div class="value-medium" style="width: 50%"><span class="stat-desc">Average</span></div></div>
                        </div>
                    </div>
                    <div class="button-holder">
                        <span class="offert-button" onclick="showMore()">More</span>
                        <span class="offert-button" >Buy</span>
                    </div>
                </div>
            </div>
        <div class="separator"></div>
        <div class="row city">
            <div class="col-md-8" id="oslo">
                <div class="row">
                    <span class="city-title">Oslo</span>
                </div>
                <div class="row">
                    <span class="flight-duration">Flight duration: 2h</span>
                </div>
                <div class="internal-separator"></div>
                <div class="row">
                    <span class="cost">Flight cost: 700 PLN</span>
                </div>
                <div class="row">
                    <span class="cost">Accomodation: avg. 400PLN/Night</span>
                </div>
            </div>
            <div class="col-md-4 text-black">
                <div>
                    <div class="row"><span class="stat-label">Avg Temp:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-bad" style="width: 20%"><span class="stat-desc">Cold</span></div></div>
                    </div>
                </div>



                <div>
                    <div class="row"><span class="stat-label">Monuments:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-good" style="width: 90%"><span class="stat-desc">A Lot</span></div></div>
                    </div>
                </div>


                <div>
                    <div class="row"><span class="stat-label">Nightlife:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-bad" style="width: 30%"><span class="stat-desc">Poor</span></div></div>
                    </div>
                </div>



                <div>
                    <div class="row"><span class="stat-label">Beach access:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-medium" style="width: 50%"><span class="stat-desc">Average</span></div></div>
                    </div>
                </div>


                <div class="button-holder">
                    <span class="offert-button" onclick="showMore()">More</span>
                    <span class="offert-button" >Buy</span>
                </div>

            </div>

        </div>


        <div class="separator"></div>


        <div class="row city">
            <div class="col-md-8" id="london">
                <div class="row">
                    <span class="city-title">London</span>
                </div>
                <div class="row">
                    <span class="flight-duration">Flight duration: 5h</span>
                </div>
                <div class="internal-separator"></div>
                <div class="row">
                    <span class="cost">Flight cost: 1500 PLN</span>
                </div>
                <div class="row">
                    <span class="cost">Accomodation: avg. 250PLN/Night</span>
                </div>
            </div>
            <div class="col-md-4 text-black">
                <div>
                    <div class="row"><span class="stat-label">Avg Temp:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-good" style="width: 80%"><span class="stat-desc">Good</span></div></div>
                    </div>
                </div>


                <div>
                    <div class="row"><span class="stat-label">Monuments:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-good" style="width: 70%"><span class="stat-desc">A Lot</span></div></div>
                    </div>
                </div>


                <div>
                    <div class="row"><span class="stat-label">Nightlife:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-good" style="width: 70%"><span class="stat-desc">Great</span></div></div>
                    </div>
                </div>




                <div>
                    <div class="row"><span class="stat-label">Beach access:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-medium" style="width: 58%"><span class="stat-desc">Average</span></div></div>
                    </div>
                </div>


                <div class="button-holder">
                    <span class="offert-button" onclick="showMore()">More</span>
                    <span class="offert-button" >Buy</span>
                </div>

            </div>

        </div>


        <div class="separator"></div>


        <div class="row city">
            <div class="col-md-8" id="paris">
                <div class="row">
                    <span class="city-title">Paris</span>
                </div>
                <div class="row">
                    <span class="flight-duration">Flight duration: 3h</span>
                </div>
                <div class="internal-separator"></div>
                <div class="row">
                    <span class="cost">Flight cost: 1100 PLN</span>
                </div>
                <div class="row">
                    <span class="cost">Accomodation: avg. 311PLN/Night</span>
                </div>
            </div>
            <div class="col-md-4 text-black">
                <div>
                    <div class="row"><span class="stat-label">Avg Temp:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-good" style="width: 50%"><span class="stat-desc">Average</span></div></div>
                    </div>
                </div>






                <div>
                    <div class="row"><span class="stat-label">Monuments:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-good" style="width: 100%"><span class="stat-desc">A Lot</span></div></div>
                    </div>
                </div>




                <div>
                    <div class="row"><span class="stat-label">Nightlife:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-good" style="width: 70%"><span class="stat-desc">Great</span></div></div>
                    </div>
                </div>






                <div>
                    <div class="row"><span class="stat-label">Beach access:</span></div>
                    <div class="row">
                        <div class="value-max-bg"><div class="value-medium" style="width: 58%"><span class="stat-desc">Average</span></div></div>
                    </div>
                </div>


                <div class="button-holder">
                    <span class="offert-button" onclick="showMore()">More</span>
                    <span class="offert-button" >Buy</span>
                </div>

            </div>

        </div>
        </div>



<div class="separator"></div>
<script>
    function showMore(){
        console.log("it works");
        let more = document.getElementById("more-popup");
        let rest = document.getElementById("bg");
        more.style.display = "block";
        rest.style.display = "block";

    }
    function closePopup(){
        let more = document.getElementById("more-popup");
        more.style.display = "none"
        let rest = document.getElementById("bg");
        rest.style.display = "none";
    }
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>